#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_clicked()
{
    exit(1);
}

void MainWindow::on_pushButton_4_clicked()
{
   // CompositeUnit adaf;
    //exit(1);
    // Нехай тепер в нормандській армії 4 легіони
    CompositeUnit* army = new CompositeUnit;
    for (int i=0; i<4; ++i)
      army->addUnit( army->createNormanArmy());

    qDebug() << "Norman army damaging strength is " << army->getStrength() << endl;
    QString damage =0;
    damage += "\nNorman army damaging strength is ";
    damage.append(QString::number(army->getStrength()));
    damage += "\n";
    ui->textEdit->append(damage);
    delete army;
}

void MainWindow::on_pushButton_3_clicked()
{
    decorator* anX = new thi_gen_decor1( new sec_gen_decor1 );
    decorator* anXY = new thi_gen_decor2( new thi_gen_decor1( new sec_gen_decor1 ) );
    decorator* anXYZ = new thi_gen_decor3( new thi_gen_decor2( new thi_gen_decor1( new sec_gen_decor1 ) ) );
    ui->textEdit->append("\nNow decorator will decorate:\n");
    ui->textEdit->append(anX->do_it());
    ui->textEdit->append(anXY->do_it());
    ui->textEdit->append(anXYZ->do_it());
    delete anX;   delete anXY;   delete anXYZ;
}

void MainWindow::on_pushButton_2_clicked()
{
    QString textt="Now you see Paris\n";
    ui->textEdit->append("Now you will see result of working of all implemented creational and structural patterns:\n");

    Singleton::getInstance();
    if(Singleton::getInstance().text1){ui->textEdit->append(textt);}

    Game game;
    EnglishArmyFactory English_factory;
    FrenchArmyFactory French_factory;

    Army * EnglishArmy = game.createArmy( English_factory);
    Army * FrenchArmy = game.createArmy( French_factory);

    ui->textEdit->append("English army:\n");
    ui->textEdit->append(EnglishArmy->info());

    ui->textEdit->append("\nFrench army:\n");
    ui->textEdit->append(FrenchArmy->info());
    ui->textEdit->append("\n");

    QVector<Prototype*> Persons;
    Person Person(" fes");

        // клонуємо об'єкт
        for (int i = 0; i < 10; ++i)
        {
            Persons.push_back
            (
                Person.Clone() // виклик методу клонування
            );
        }
        // Друкуємо клонів
        for (int i = 0; i < 10; ++i)
        {
            Person.print(Persons[i]);
        }
    ui->textEdit->append("Civillian people in Paris:\n");
    ui->textEdit->append(Person.var);
}

void MainWindow::on_pushButton_5_clicked()
{
    ui->textEdit->setText(text_game_state.quick_load());
}

void MainWindow::on_pushButton_6_clicked()
{
    QString text = ui->textEdit->toPlainText();
    text_game_state.quick_save(text);

    ofstream myfile;
    myfile.open ("E:\\Qt\\Projectu\\2kyrs2semestr\\Laba8\\seria.bin");
    QByteArray ba = text_game_state.game.text.toLocal8Bit();
    const char *c_str2 = ba.data();
    myfile.write(c_str2, sizeof(text_game_state));
    myfile.close();

    /*QString filename="E:\\Qt\\Projectu\\2kyrs2semestr\\Laba8\\seria.bin";
    QFile file( filename );
    if ( file.open(QIODevice::ReadWrite) )
    {
        QTextStream stream( &file );
        stream << text_game_state.game.text << endl;
    }*/

    /* Try and open a file for output
    QString outputFilename = "E:\\Qt\\Projectu\\2kyrs2semestr\\Laba8\\seria.txt";
    QFile outputFile(outputFilename);
    outputFile.open(QIODevice::WriteOnly);
    /* Check it opened OK */
    /*if(!outputFile.isOpen()){
        qDebug()  << "- Error, unable to open" << outputFilename << "for output";
        //return 1;
    }*/
    /* Point a QTextStream object at the file */
    //QTextStream outStream(&outputFile);
    /* Write the line to the file */
    //outStream << text_game_state.game.text;
    /* Close the file */
    //outputFile.close();

    /*QString path= "E:\\Qt\\Projectu\\2kyrs2semestr\\Laba8\\seria.bin";
    QFile file(path);
    QString mystring = text_game_state.game.text;
    if(!file.open(QIODevice::WriteOnly)){
            file.close();
        } else {
            QTextStream out(&file); out << mystring;
            file.close();
        }*/
}

void MainWindow::on_pushButton_7_clicked()
{
    //std::fstream input( "E:\\Qt\\Projectu\\2kyrs2semestr\\Laba8\\seria.bin", std::ios::binary );

    QString line;

    QFile inputFile("E:\\Qt\\Projectu\\2kyrs2semestr\\Laba8\\seria.bin");
    if (inputFile.open(QIODevice::ReadOnly))
    {
       QTextStream in(&inputFile);
       while (!in.atEnd())
       {
         line = in.readLine();
       }
       inputFile.close();
    }

    ui->textEdit->setText(line);
}

void MainWindow::on_pushButton_8_clicked()
{
    ui->textEdit->setText("");
}
