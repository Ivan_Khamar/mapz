#ifndef COMPOSITE_H
#define COMPOSITE_H

#include <iostream>
#include <vector>
#include <assert.h>
#include <QString>

// Клас екземпляра
class Unit
{
  public:
    virtual int getStrength() = 0;
    virtual void addUnit(Unit* p)
    {
      assert( false);
    }
    virtual ~Unit() {}
};

// Класи
class Archer2: public Unit
{
  public:
    virtual int getStrength()
    {
      return 1;
    }
};

class Infantryman2: public Unit
{
  public:
    virtual int getStrength()
    {
      return 2;
    }
};

class Horseman2: public Unit
{
  public:
    virtual int getStrength()
    {
      return 3;
    }
};

// Компонувальник
class CompositeUnit: public Unit
{
  public:
    int getStrength()
    {
      int total = 0;
      for(int unsigned i=0; i<c.size(); ++i)
        total += c[i]->getStrength();
      return total;
    }
    void addUnit(Unit* p)
    {
        c.push_back( p);
    }
    ~CompositeUnit()
    {
      for(int unsigned i=0; i<c.size(); ++i)
      delete c[i];
    }
    CompositeUnit* createNormanArmy()
    {
        {// в армії норманів:
          CompositeUnit* normanArmy;
          normanArmy = new CompositeUnit;
          // 3000 тяжкої піхоти
          for (int i=0; i<3000; ++i)
            normanArmy->addUnit(new Infantryman2);
          // 1200 легкої піхоти
          for (int i=0; i<1200; ++i)
            normanArmy->addUnit(new Archer2);
          // 300 вершників
          for (int i=0; i<300; ++i)
            normanArmy->addUnit(new Horseman2);

          return normanArmy;
        }
    };
  private:
   std::vector<Unit*> c;
};

#endif // COMPOSITE_H
