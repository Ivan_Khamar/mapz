#include "facade.h"
#include "ui_facade.h"

facade::facade(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::facade)
{
    ui->setupUi(this);
}

facade::~facade()
{
    delete ui;
}

void facade::on_pushButton_2_clicked()
{
    exit(1);
}

void facade::on_pushButton_clicked()
{
    QString textt="Now you see Paris\n";
    ui->textEdit->append("Now you will see result of working of all implemented creational and structural patterns:\n");

    Singleton::getInstance();
    if(Singleton::getInstance().text1){ui->textEdit->append(textt);}

    Game game;
    EnglishArmyFactory English_factory;
    FrenchArmyFactory French_factory;

    Army * EnglishArmy = game.createArmy( English_factory);
    Army * FrenchArmy = game.createArmy( French_factory);

    ui->textEdit->append("English army:\n");
    ui->textEdit->append(EnglishArmy->info());

    ui->textEdit->append("\nFrench army:\n");
    ui->textEdit->append(FrenchArmy->info());
    ui->textEdit->append("\n");

    QVector<Prototype*> Persons;
    Person Person(" fes");

        // клонуємо об'єкт
        for (int i = 0; i < 10; ++i)
        {
            Persons.push_back
            (
                Person.Clone() // виклик методу клонування
            );
        }
        // Друкуємо клонів
        for (int i = 0; i < 10; ++i)
        {
            Person.print(Persons[i]);
        }
    ui->textEdit->append("Civillian people in Paris:\n");
    ui->textEdit->append(Person.var);
}

void facade::on_pushButton_3_clicked()
{
       decorator* anX = new thi_gen_decor1( new sec_gen_decor1 );
       decorator* anXY = new thi_gen_decor2( new thi_gen_decor1( new sec_gen_decor1 ) );
       decorator* anXYZ = new thi_gen_decor3( new thi_gen_decor2( new thi_gen_decor1( new sec_gen_decor1 ) ) );
       ui->textEdit->append("\nNow decorator will decorate:\n");
       ui->textEdit->append(anX->do_it());
       ui->textEdit->append(anXY->do_it());
       ui->textEdit->append(anXYZ->do_it());
       delete anX;   delete anXY;   delete anXYZ;
}

void facade::on_pushButton_4_clicked()
{
    // Нехай тепер в англійській армії 4 легіони
      CompositeUnit* army = new CompositeUnit;
      for (int i=0; i<4; ++i)
        army->addUnit( army->createNormanArmy());

      qDebug() << "Norman army damaging strength is " << army->getStrength() << endl;
      QString damage =0;
      damage += "\nNorman army damaging strength is ";
      damage.append(QString::number(army->getStrength()));
      damage += "\n";
      ui->textEdit->append(damage);
      delete army;
}
