#ifndef FACADE_H
#define FACADE_H

#include <QMainWindow>
#include <singleton.h>
#include <abstractFactory.cpp>
#include <prototype.h>
#include <decorator.h>
#include <composite.h>

namespace Ui {
class facade;
}

class facade : public QMainWindow
{
    Q_OBJECT

public:
    explicit facade(QWidget *parent = nullptr);
    ~facade();

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

private:
    Ui::facade *ui;
};

#endif // FACADE_H
