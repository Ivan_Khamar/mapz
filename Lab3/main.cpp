#include "facade.h"
#include "mainwindow1.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    facade w;
    w.show();
    return a.exec();
}
