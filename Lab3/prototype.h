//#ifndef PROTOTYPE_H
//#define PROTOTYPE_H

#include <iostream>
#include <QString>
#include <QRandomGenerator>
#include <QDebug>

using namespace std;

struct Prototype
{
    virtual Prototype* Clone() = 0;
};

class Person : public Prototype
{
public:
    int Age;
    string Name;
public:
    Person(string strName) : Name(strName)
    {
        Age = GetNewAge();
        Name = GetNewName();
    };
    // конструктор копіювання
    Person(Person& Person) :  Name(Person.Name)
    {
        Age = GetNewAge();
        Name = GetNewName();
    };
    virtual ~Person(){};
    virtual Prototype* Clone() {return new Person(*this);};
    //QRandomGenerator rand;
    int GetNewAge()
    {
        std::uniform_int_distribution<int> distribution(1,100);
        for(int i=0;i<10;i++)
        {
          int Age = distribution(*QRandomGenerator::global());
          return Age;
        }
    };
    string masName[11] = {"Julien","Lorie","Gregoire","Christelle","Philippe","Charlotte","Pierre","Jocelyne","Louis","Ariane"};
    //string masName[11] = {"Andriy","Ivan","Maria","Anna","Andriy","Ivan","Maria","Anna","Andriy","Ivan","Yura"};
    string GetNewName()
    {
        std::uniform_int_distribution<int> distribution1(1,10);
        for(int i=0;i<10;i++)
        {
          int NameNum = distribution1(*QRandomGenerator::global());
          string Name = masName[NameNum];
          return Name;
        }
    };


    QString var = 0;

    QString print(Prototype* p)
    {
        Person* pPerson = dynamic_cast <Person*> (p);

        var += "Name: ";
        var += QString::fromStdString(pPerson->Name);
        var += "\n";
        var += "Age: ";
        var.append(QString::number(pPerson->Age));
        var += "\n";
        return var;
    };
};

//#endif // PROTOTYPE_H
