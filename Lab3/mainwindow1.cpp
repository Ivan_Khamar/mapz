#include "mainwindow1.h"
#include "ui_mainwindow.h"
#include <singleton.h>
#include <abstractFactory.cpp>
#include <prototype.h>

QString textt="\nNow you see Paris\n";

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_clicked()
{
    Singleton::getInstance();
    if(Singleton::getInstance().text1){ui->textEdit->setText(textt);}
}

void MainWindow::on_pushButton_4_clicked()
{
    exit(1);
}

void MainWindow::on_pushButton_2_clicked()
{
    Game game;
    EnglishArmyFactory English_factory;
    FrenchArmyFactory French_factory;

    Army * EnglishArmy = game.createArmy( English_factory);
    Army * FrenchArmy = game.createArmy( French_factory);

    ui->textEdit_2->append("English army:\n");
    ui->textEdit_2->append(EnglishArmy->info());

    ui->textEdit_2->append("\nFrench army:\n");
    ui->textEdit_2->append(FrenchArmy->info());
}

void MainWindow::on_pushButton_3_clicked()
{
    QVector<Prototype*> Persons;
    Person Person(" fes");

        // клонуємо об'єкт
        for (int i = 0; i < 10; ++i)
        {
            Persons.push_back
            (
                Person.Clone() // виклик методу клонування
            );
        }
        // Друкуємо клонів
        for (int i = 0; i < 10; ++i)
        {
            Person.print(Persons[i]);
        }
    ui->textEdit_3->append("Civillian people in Paris:\n");
    ui->textEdit_3->append(Person.var);
}
