#ifndef DECORATOR_H
#define DECORATOR_H

#include <QDebug>
#include <QString>

class decorator
{
public:
    decorator();
    virtual ~decorator() { }
    virtual QString do_it() = 0;
    QString name;
};

class sec_gen_decor1 : public decorator {
public:
   ~sec_gen_decor1() { qDebug() << "sec_gen_decor1 in decorator\n";}
   /*virtual*/ QString do_it() { qDebug()<<"sec_gen_decor1\n"; return name+="stone sec_gen_decor1\n"; }
};

class sec_gen_decor2 : public decorator {
public:
   sec_gen_decor2( decorator* inner ){ dec_wrappee = inner; }
   ~sec_gen_decor2(){delete dec_wrappee;}
   /*virtual*/ QString do_it() {return dec_wrappee->do_it(); }
protected:
   decorator*  dec_wrappee;
};

class thi_gen_decor1 : public sec_gen_decor2 {
public:
   thi_gen_decor1( decorator* core ) : sec_gen_decor2( core ) { }
   ~thi_gen_decor1() { qDebug() << "thi_gen_decor1\n" ;}
   /*virtual*/ QString do_it() {
      sec_gen_decor2::do_it();
      qDebug() << "thi_gen_decor1\n";
      return name=" woods ";
   }
};

class thi_gen_decor2 : public sec_gen_decor2 {
public:
   thi_gen_decor2( decorator* core ) : sec_gen_decor2( core ) { }
   ~thi_gen_decor2() { qDebug() << "thi_gen_decor2\n";}
   /*virtual*/ QString do_it() {
      sec_gen_decor2::do_it();
      qDebug() << "thi_gen_decor2\n";
      return name+=" village ";
   }
};

class thi_gen_decor3 : public sec_gen_decor2 {
public:
   thi_gen_decor3( decorator* core ) : sec_gen_decor2( core ) { }
   ~thi_gen_decor3() { qDebug() << "thi_gen_decor3\n";}
   /*virtual*/ QString do_it() {
      sec_gen_decor2::do_it();
      qDebug() << "thi_gen_decor3\n";
      return name+=" fields ";
   }
};

#endif // DECORATOR_H
