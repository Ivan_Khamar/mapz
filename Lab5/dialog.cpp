#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_pushButton_clicked()
{
    close();
}

void Dialog::on_pushButton_2_clicked()
{
    ui->textEdit->clear();
}

void Dialog::on_pushButton_3_clicked()
{
    ConcreteMediator m;
    French_Army c2(&m);
    m.SetArmy2(&c2);
    ui->textEdit->append(c2.Notify("\nThat was very rude, you frogeaters! We are leaving!"));
}
