#ifndef MEMENTO_H
#define MEMENTO_H

#include <iostream>
#include <array>
#include <Windows.h>
#include <QString>

using namespace std;

// Стан містить стан
struct State
{
    float health;
    int amount_of_killed;
    State() :health(100), amount_of_killed(0) {};
    friend ostream& operator<<(ostream & os, const State & s)
    {
        return os << "Alives left: " << s.health << " , killed " << s.amount_of_killed << '\n';
    }
};
// Memento — контекст
// зберігає внутрішній стан об'єкта Originator
class GameMemento
{
private:
//    State state;
      QString text;
public:
    GameMemento() {}
    GameMemento(QString text) :text(text) {}
    QString get_text()
    {
        return text;
    }
    void set_text(QString value)
    {
        text = value;
    }
};
// Originator — хазяїн
// створює знімок, що утримує поточний внутрішній стан;
// використовує знімок для відтворення внутрішнього стану;
class GameOriginator
{
private:
//    Стан містить здоров’я та кількість убитих монстрів
      State state;

public:
    void play()
    {
        // Імітуємо процес гри —
        state.health *= 0.9;
        state.amount_of_killed += 2;
        cout << state;
    }
    QString text;
    GameMemento game_save(QString text)
    {
        //QString text1;
        return GameMemento(text);
    }
    void load_game(GameMemento memento)
    {
        text = memento.get_text();
    }
};
//CareTaker — опікун
//відповідає за зберігання знімку;
//не проводить жодних операцій над знімком та не має уяви про його внутрішній зміст.
class Caretaker
{
private:

    array< GameMemento, 5 > quick_saves;
    int k;
    bool save_load;
    void check_save()
    {
        k < 5 ? k++ : k = 0;
    }
    // метод аби побачити коли була здійснене завантаження
    void check_load()
    {
        if (save_load == true)
        {
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_GREEN);
            save_load = false;
        }
        else
        {
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
        }
    }
public:
    GameOriginator game;
    Caretaker() : k(-1) {}
    void shoot()
    {
        check_load();
        //game.play();
    }
    void quick_save(QString text)
    {
        check_save();
        quick_saves[k] = game.game_save(text);
    }
    QString quick_load(int i = -1)
    {
        if (i < 0 || i > 5) i = k;
        game.load_game(quick_saves.at(i));
        save_load = true;
        return game.text;
    }
    void clear()
    {
        //array< GameMemento, 5 > quick_saves2;
//        QString new_text = " ";
//        GameMemento empty_memento = GameMemento(new_text);
//        quick_saves.fill(empty_memento);
        k = 0;

        for (int i=0; i < quick_saves.size();i++) {
            //var.set_text(QString(""));
            quick_saves[i].set_text(QString(""));
            //quick_saves.
        }
    }
};

#endif // MEMENTO_H
