#ifndef MEDIATOR_H
#define MEDIATOR_H

#include <iostream>
#include <QString>
#include <QDebug>

using namespace std;

class Colleague;
class Mediator;
class ConcreteMediator;
class English_Army;
class French_Army;
struct Mediator
{
    virtual void Send(QString const& message, Colleague *colleague) const = 0;
};
class Colleague
{
protected:
    Mediator* mediator;
public:
    explicit Colleague(Mediator *mediator) :mediator(mediator) {}
};
struct English_Army :public Colleague
{
    explicit English_Army(Mediator* mediator) :Colleague(mediator) {}
    void Send(QString const& message)
    {
        mediator->Send(message, this);
    }
    QString Notify(QString const& message)
    {
        QString text2 = "French army send message:";
        text2.append(message);
        return text2;
    }
};
struct French_Army :public Colleague
{
    explicit French_Army(Mediator *mediator) :Colleague(mediator) {}
    void Send(QString const& message)
    {
        mediator->Send(message, this);
    }
    QString Notify(QString const& message)
    {
        QString text1 = "English army send message: ";
        text1.append(message);
        return text1;
    }
};
class ConcreteMediator :public Mediator
{
protected:
    English_Army * m_Army1;
    French_Army * m_Army2;
public:
    void SetArmy1(English_Army *c)
    {
        m_Army1 = c;
    }
    void SetArmy2(French_Army *c)
    {
        m_Army2 = c;
    }
    virtual void Send(QString const& message, Colleague *colleague) const
    {
        if (colleague == m_Army1)
        {
            m_Army2->Notify(message);
        }
        else if (colleague == m_Army2)
        {
            m_Army1->Notify(message);
        }
    }
};

#endif // MEDIATOR_H
