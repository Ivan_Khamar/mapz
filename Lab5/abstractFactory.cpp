#include <iostream>
#include <QVector>
#include <QString>

//Тут віртуальні класи для всіх воїнів
class Infantryman
{
  public:
    QString infantryman;
    virtual QString info() = 0;
    virtual ~Infantryman() {}
};

class Archer
{
  public:
    QString archer;
    virtual QString info() = 0;
    virtual ~Archer() {}
};

class Horseman
{
  public:
    QString horseman;
    virtual QString info() = 0;
    virtual ~Horseman() {}
};

//Всі воїни Англії
class EnglishInfantryman: public Infantryman
{
  public:
    QString infantryman = "--> English Infantryman";
    QString info()
    {
      return infantryman;
    }
};

class EnglishArcher: public Archer
{
  public:
    QString archer = "--> English Archer";
    QString info()
    {
      return archer;
    }
};

class EnglishHorseman: public Horseman
{
  public:
    QString horseman = "--> English Horseman";
    QString info()
    {
        return horseman;
    }
};

//Всі воїни Франції
class FrenchInfantryman: public Infantryman
{
  public:
    QString infantryman = "--> French Infantryman";
    QString info()
    {
        return infantryman;
    }
};

class FrenchArcher: public Archer
{
  public:
    QString archer = "--> French Archer";
    QString info()
    {
        return archer;
    }
};

class FrenchHorseman: public Horseman
{
  public:
    QString horseman = "--> French Horseman";
    QString info()
    {
        return horseman;
    }
};

//Віртуальна фабрика для армій
class ArmyFactory
{
  public:
    virtual Infantryman* createInfantryman() = 0;
    virtual Archer* createArcher() = 0;
    virtual Horseman* createHorseman() = 0;
    virtual ~ArmyFactory() {}
};

//Фабрика армії Англії
class EnglishArmyFactory: public ArmyFactory
{
  public:
    Infantryman* createInfantryman() {
      return new EnglishInfantryman;
    }
    Archer* createArcher() {
      return new EnglishArcher;
    }
    Horseman* createHorseman() {
      return new EnglishHorseman;
    }
};

//Фабрика армії Франції
class FrenchArmyFactory: public ArmyFactory
{
  public:
    Infantryman* createInfantryman() {
      return new FrenchInfantryman;
    }
    Archer* createArcher() {
      return new FrenchArcher;
    }
    Horseman* createHorseman() {
      return new FrenchHorseman;
    }
};

//Клас для зберігання воїнів в певній армії
class Army
{
  public:
    QString army=0;
   ~Army() {
      int i;
      for(i=0; i<InfantryVector.size(); ++i)  delete InfantryVector[i];
      for(i=0; i<ArcherVector.size(); ++i)  delete ArcherVector[i];
      for(i=0; i<HorsemanVector.size(); ++i)  delete HorsemanVector[i];
    }
    QString info() {
      int i;
      for(i=0; i<InfantryVector.size(); ++i)  army += InfantryVector[i]->info();
      army += "\n";
      for(i=0; i<ArcherVector.size(); ++i)  army += ArcherVector[i]->info();
      army += "\n";
      for(i=0; i<HorsemanVector.size(); ++i)  army += HorsemanVector[i]->info();
      return army;
    }
    QVector<Infantryman*> InfantryVector;
    QVector<Archer*> ArcherVector;
    QVector<Horseman*> HorsemanVector;
};


//Базовий клас для зберігання армій
class Game
{
  public:
    Army* createArmy( ArmyFactory& factory ) {
      Army* p = new Army;
      p->InfantryVector.push_back( factory.createInfantryman());
      p->ArcherVector.push_back( factory.createArcher());
      p->HorsemanVector.push_back( factory.createHorseman());
      return p;
    }
};
