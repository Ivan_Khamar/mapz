#ifndef COMMAND_H
#define COMMAND_H

#include <iostream>
#include <QVector>
#include <QDebug>

using namespace std;

struct Command // Основа шаблону
{
    virtual QString execute() = 0;
    QString textCommand;
};
// Уточнення
struct Ask_negotiations : public Command
{
    QString textCommand = "\nNegotiations has been asked:";
    virtual QString execute() {return textCommand;};
};
struct Agree_to_negotiate : public Command
{
    QString textCommand = "Armies agreed to negotiate";
    virtual QString execute() {return textCommand;};
};
struct Negotiations_started : public Command
{
    QString textCommand = "Negotiations successfully started";
    virtual QString execute()
    {
        return textCommand;
    }
};
// Місце, де застосовують команду
class Macro
{
public:
    QVector< Command*> commands;
    void add(Command* c) { commands.push_back(c); }
    void run()
    {
        QVector< Command*> ::iterator it = commands.begin();
        while (it != commands.end()) (*it++)->execute();
    }
};

#endif // COMMAND_H
